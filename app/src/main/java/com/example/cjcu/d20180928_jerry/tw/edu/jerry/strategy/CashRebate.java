package com.example.cjcu.d20180928_jerry.tw.edu.jerry.strategy;

//打x折
public class CashRebate extends CashSuper {

    //折扣
    private double moneyRebate = 1;

    public CashRebate(double moneyRebate)
    {
        this.moneyRebate = moneyRebate;
    }

    @Override
    public double acceptCash(double money) {
        return money * this.moneyRebate;
    }
}

package com.example.cjcu.d20180928_jerry;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.cjcu.d20180928_jerry.tw.edu.jerry.strategy.CashContent;
import com.example.cjcu.d20180928_jerry.tw.edu.jerry.strategy.CashRandom;
import com.example.cjcu.d20180928_jerry.tw.edu.jerry.strategy.CashRebate;
import com.example.cjcu.d20180928_jerry.tw.edu.jerry.strategy.CashReturn;
import com.example.cjcu.d20180928_jerry.tw.edu.jerry.strategy.CashSuper;

public class SecondActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }

    public void getReusult(View view) {
        EditText quantity = (EditText) findViewById(R.id.edit_Quantity);
        EditText unitPrice = (EditText) findViewById(R.id.edit_price);

        double quant = Double.parseDouble(quantity.getText().toString());
        double unit = Double.parseDouble(unitPrice.getText().toString());

        TextView et_NumA = (TextView) findViewById(R.id.tv_Amount);

        CashSuper cs = new CashRebate(0.8);

        cs = new CashReturn(1000,50);

        cs = new CashRandom();

        //策略模式
        CashContent cc = new CashContent(cs);


        et_NumA.setText(String.valueOf(cc.GetResult(quant * unit)));

    }
}

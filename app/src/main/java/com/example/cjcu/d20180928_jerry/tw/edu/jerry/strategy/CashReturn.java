package com.example.cjcu.d20180928_jerry.tw.edu.jerry.strategy;

//現金回饋
public class CashReturn extends CashSuper {

    private int moneycondition = 0;
    private int moneyReturn = 0;

    public CashReturn(int moneycondition , int moneyReturn)
    {
        this.moneycondition = moneycondition;
        this.moneyReturn = moneyReturn;
    }



    @Override
    public double acceptCash(double money) {

        double count = Math.floor(money / moneycondition);


        double result = money - moneyReturn * count;

        return result;
    }
}

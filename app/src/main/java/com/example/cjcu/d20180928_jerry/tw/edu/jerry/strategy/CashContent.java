package com.example.cjcu.d20180928_jerry.tw.edu.jerry.strategy;

public class CashContent {
    private CashSuper cs;
    public CashContent(CashSuper cs)
    {
        this.cs = cs;
    }

    public double GetResult(double money)
    {
        return cs.acceptCash(money);
    }
}

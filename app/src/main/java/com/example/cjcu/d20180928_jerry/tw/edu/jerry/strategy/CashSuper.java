package com.example.cjcu.d20180928_jerry.tw.edu.jerry.strategy;

public abstract class CashSuper {
    public abstract double acceptCash(double money);
}

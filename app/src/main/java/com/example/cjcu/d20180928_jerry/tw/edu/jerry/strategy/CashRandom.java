package com.example.cjcu.d20180928_jerry.tw.edu.jerry.strategy;

import android.util.Log;

public class CashRandom extends CashSuper{
    @Override
    public double acceptCash(double money) {
        double random = Math.random()*100;

        Log.d("Jerry",String.valueOf(random));

        if(random < 10)
        {
            return 0;
        }
        else if(random < 30)
        {
            return money * 0.3;
        }
        else if(random < 80)
        {
            return money * 0.9;
        }
        else
        {
            return money;
        }
    }
}
